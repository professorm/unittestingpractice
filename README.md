# testingbasics
Start by forking this repo so that you have your own copy on BitBucket. 
Then clone the remote repo so you download it to your own machine.

For this exercise:
1.  Choose one method
2.  Write 3 test cases for that method.  Two should test success conditions, one should test a failure condition. 
3.  Setup and breakdown each test case if needed
